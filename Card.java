public class Card{
	
	private String suit;
	private int value;
	
	public Card( int value, String suit){
		this.suit=suit;
		this.value=value;
	}
	
	public int getValue(){
		return this.value;
	}
	
	public String getSuit(){
		return this.suit;
	}
	
	public String toString(){
		String printValue = Integer.toString(value);
		if (value == 1){
			printValue= "Ace";
		}else if (value == 11){
			printValue= "Jack";
		}else if (value == 12){
			printValue = "Queen";
		}else if (value == 13){
			printValue = "King";
		}
		return printValue + " of " + suit;
	}
	public double calculateScore(){
		double score = value;
		if (suit.equals("Hearts")){
			score+=0.4;
		}else if (suit.equals("Spades")){
			score += 0.3;
		}else if (suit.equals("Diamonds")){
			score += 0.2;
		}else if (suit.equals("Clubs")){
			score += 0.1;
		}else if (value == 1){ //ace
		score += 1.0;
			}else if (value == 2){
			score += 2.0;
			}else if (value == 3){
			score += 3.0;
			}else if (value == 4){
			score += 4.0;
			}else if (value == 5){
			score += 5.0;
			}else if (value == 6){
			score += 6.0;
			}else if (value == 7){
			score += 7.0;
			}else if (value == 8){
			score += 8.0;
			}else if (value == 9){
			score += 9.0;
			}else if (value == 10){
			score += 10.0;
			}else if (value == 11){
			score += 11.0;
			}else if (value == 12){
			score += 12.0;
			}else if (value == 13){
			score += 13.0;
			}
				return score;
	}
}