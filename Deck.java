import java.util.Random;

public class Deck {

    private Card[] cards;
    private int lastCardPos;
    private Random rng;
    private String[] suit = {"Hearts", "Diamonds", "Clubs", "Spades"};
    private int[] value = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13};

    public Deck() {
        rng = new Random();
        cards = new Card[52];
        int i = 0;

        for (String s : suit) {
            for (int v : value) {
                cards[i] = new Card(v, s);
                i++;
            }
        }
        lastCardPos = cards.length; // Initialize the number of cards
    }

    public int length() {
        return lastCardPos;
    }

    public Card drawTopCard() {
        if (lastCardPos <= 0) {
            return null; // Deck is empty
        }
        lastCardPos--;
        return cards[lastCardPos];
    }

    public String toString() {
	String result= "";
        for (int i = 0; i < lastCardPos; i++) {
            result += cards[i].toString() + "\n";
        }
        return result;
    }

    public void shuffle() {
        for (int i = 0; i < lastCardPos - 1; i++) {
            int randomIndex = i + rng.nextInt(lastCardPos - i);
            Card temp = cards[i];
            cards[i] = cards[randomIndex];
            cards[randomIndex] = temp;
        }
    }
}
