public class SimpleWar{
	public static void main(String[] args){
	Deck stack = new Deck();
	stack.shuffle();
	
	int playerOne=0;
	int playerTwo=0;
	int roundNumber=1;
	
	
	//for (int round=1; round <=5; round ++){
		//System.out.println("*********************************** \n");
		//System.out.println("Round " + round + ": \n");
		
		while (stack.length() > 0){
		System.out.println("*********************************** \n");
		System.out.println("Round " + roundNumber); // Print round number
		roundNumber++;
		Card card1= stack.drawTopCard();
		Card card2= stack.drawTopCard();
	//testing if shuffled deck works
	//System.out.println("shuffled deck : \n" + stack);
	
	//testing drawing cards for each player
	//System.out.println("Player 1's first card: \n" + stack.drawTopCard());
	//System.out.println("Player 2's first card: \n" + stack.drawTopCard());
	

	System.out.println("Player 1's first card: \n" + card1 + " " + card1.calculateScore());
	System.out.println("\n");
	System.out.println("Player 2's first card: \n" + card2 + " " + card2.calculateScore());
	System.out.println("\n");
	
	if(card1.calculateScore() > card2.calculateScore()){
		System.out.println("Player 1 wins!");
		playerOne++;
	}else if(card2.calculateScore() > card1.calculateScore()){
		System.out.println("Player 2 wins!");
		playerTwo++;
	}else{
		System.out.println(" Its a tie!");
		}
		System.out.println("Player 1 points: " + playerOne);
		System.out.println("Player 2 points: " + playerTwo);
	}
	if ( playerOne > playerTwo){
		System.out.println("Congratulations Player 1 wins with: " + playerOne + " points!");
	}else{
		System.out.println("Congratulations Player 2 wins with: " + playerTwo + " points!");
	
	}
}
	}